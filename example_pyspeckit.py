import numpy as np
import pyspeckit
 #Gnerating the gaussian. That we eill fit with pyspeckit
xaxis = np.linspace(-50,150,100.)
sigma = 10.
center = 50.
synth_data = np.exp(-(xaxis-center)**2/(sigma**2 * 2.))

# Add noise
stddev = 0.05
noise = np.random.randn(xaxis.size)*stddev
error = np.abs(stddev*np.random.randn(len(synth_data)))
conti=0.05*xaxis+0.5
data = noise+synth_data+conti

# this will give a "blank header" warning, which is fine
sp = pyspeckit.Spectrum(data=data, error=error, xarr=xaxis,
                        xarrkwargs={'unit':'km/s'},
                        unit='erg/s/cm^2/AA')

exclude_cont=[25,75] # there are the regions tha define themessionline
backup=sp.copy()
backup.plotter()
backup.baseline.powelow=False
backup.baseline(xmin=-45,xmax=95,exclude=exclude_cont, subtract=False, highlight_fitregion=False,powerlaw=False,interactive=False,quiet=False,LoudDebug=False,annotate=False)
sp.data=sp.data-backup.baseline.basespec
# Fit with automatic guesses
sp.plotter()

# Fit with input guesses
# The guesses initialize the fitter
# This approach uses the 0th, 1st, and 2nd moments
amplitude_guess = sp.data.max()
center_guess = (sp.data*xaxis).sum()/sp.data.sum()
width_guess = 18.0
guesses = [amplitude_guess, center_guess, width_guess]
limits=[(0,1),(30,70),(2,30)]
limited=[(True,False),(True,True),(True,True)]
fixed = [False, False, False]
tied=["","",""]


p.plotter()
sp.specfit(fittype='gaussian', guesses=guesses)
sp.specfit(fittype='gaussian', guesses=guesses,limits=limits,limited=limited,fixed=fixed,tied=tied)
sp.plotter(errstyle='fill')
sp.specfit.plot_fit()


#Adding a second gaussion

sigma2=2
Amp=3
shift=3.0
center2=center+shift
synth_data2 = Amp*np.exp(-(xaxis-center2)**2/(sigma2**2 * 2.))
data2=noise+synth_data+conti+synth_data2
sp2 = pyspeckit.Spectrum(data=data2, error=error, xarr=xaxis,
                        xarrkwargs={'unit':'km/s'},
                        unit='erg/s/cm^2/AA')
exclude_cont=[25,75]
backup1=sp2.copy()
backup1.plotter()
backup1.baseline.powelow=False
backup1.baseline(xmin=-45,xmax=95,exclude=exclude_cont, subtract=False, highlight_fitregion=False,powerlaw=False,interactive=False,quiet=False,LoudDebug=False,annotate=False)
sp2.data=sp2.data-backup1.baseline.basespec
amplitude_guess1 = sp2.data.max()
center_guess1 = center_guess
width_guess1=4.0
guesses = [amplitude_guess, center_guess, width_guess,amplitude_guess1, center_guess1, width_guess1/2.0]
guesses = [1,60,6,1,60,4]
limits=[(0,1),(30,70),(5,30),(0,1),(30,70),(0,5)]
limited=[  (True,False),(True,True),(True,True),     (True,False),(True,True),(True,True)   ]
fixed = [False, False, False, False,False,False]

tied=["","","","","p[1]+%g"%(shift),""]
#tied=["","","","","",""]
sp2.plotter()
sp2.specfit(fittype='gaussian', guesses=guesses,limits=limits,limited=limited,fixed=fixed,tied=tied)
sp2.plotter(errstyle='fill')
sp2.specfit.plot_fit()
sp2.specfit.plot_components(add_baseline=False)
